/*
 * project
 * http://www.reasondigital.com/
 *
 * Initialise deployment and project scaffolding for a new project.
 *
 * Includes:
 *   > Jenkins-ready folder structure
 *   > Grunt-ready Underscores theme
 */

// Basic template description.
exports.description = 'Initialise deployment and project scaffolding for a new project.';

// Away we go!
exports.template = function(grunt, init, done) {
	'use strict';

	grunt.file.preserveBOM = true;

	init.process({}, [
		// Prompt for these values
		init.prompt('localurl', 'Your full local project URL, e.g. http://myproject.local.reason.digital'),
		init.prompt('themename', 'Your full project name, e.g. My Project'),
		init.prompt('themeslug', 'Your project folder name, e.g. rd-my-project'),
		init.prompt('description', 'Your project description, e.g. The My Project theme for WordPress')
	], function(err, props) {
		var namespace = props.themeslug;

		// Set paths
		props.srcdir = 'src/' + props.themeslug;
		props.distdir = 'htdocs/wp-content/themes/';
		props.gruntpath = props.distdir + props.themeslug;
		props.namespace = namespace.replace(/-/g, '_');

		// Files to copy and process
		var dest,
			destFiles = [],
			files = init.filesToCopy(props),
			pkgProps = {
				name: props.themeslug,
				version: '0.5.0',
				devDependencies: {
					"grunt": "latest",
					"grunt-contrib-clean": "latest",
					"grunt-contrib-concat": "latest",
					"grunt-contrib-uglify": "latest",
					"grunt-contrib-jshint": "latest",
					"grunt-contrib-compass": "latest",
					"grunt-contrib-cssmin": "latest",
					"grunt-contrib-imagemin": "latest",
					"grunt-uncss": "0.3.8",
					"grunt-penthouse": "latest",
					"grunt-svgmin": "latest",
					"grunt-contrib-watch": "latest",
					"jit-grunt": "latest"
				}
			};

		Object.prototype.renameProperty = function(oldName, newName) {
			if (this.hasOwnProperty(oldName)) {
				this[newName] = this[oldName];

				delete this[oldName];
			}

			return this;
		};

		for (var key in files) {
			dest = key.replace(/my-project/g, props.themeslug);

			files.renameProperty(key, dest);
		}

		// All finished, do something with the properties
		init.copyAndProcess(files, props);

		// Create Jenkins directory structure, files for deployment
		// and source directory
		var dirs = [
				'/db/',
				'/deploy/dev/htdocs/',
				'/deploy/dev/scripts/',
				'/deploy/stage/htdocs/',
				'/deploy/stage/scripts/',
				'/deploy/live/htdocs/',
				'/deploy/live/scripts/',
				'/src/' + props.themeslug + '/css/lib/',
				'/src/' + props.themeslug + '/js/lib/',
				'/src/' + props.themeslug + '/sass/lib/',
				'/src/' + props.themeslug + '/svg/',
				'/src/' + props.themeslug + '/images/',
				'/src/' + props.themeslug + '/fonts/'
			],
			deployFiles = {
				exclusions: [
					'exclusions.txt', ''
				],
				robots: [
					'robots.txt', 'User-agent: *\nDisallow: /'
				]
			};

		for (var i = 0, len = dirs.length; i < len; i++) {
			var dirArr = dirs[i].split('/'),
				dirPath = init.destpath() + dirs[i];

			grunt.file.mkdir(dirPath);

			// exclusions.txt
			if (dirArr.indexOf('scripts') !== -1) {
				grunt.file.write(dirPath + deployFiles.exclusions[0], deployFiles.exclusions[1]);
			}

			// robots.txt
			if ((dirArr.indexOf('dev') !== -1 || dirArr.indexOf('stage') !== -1) && dirArr.indexOf('htdocs') !== -1) {
				grunt.file.write(dirPath + deployFiles.robots[0], deployFiles.robots[1]);
			}
		}

		// Generate package.json file, used by npm and Grunt
		init.writePackageJSON('package.json', pkgProps, function(pkg, pkgProps) {
			// Ensure package is set to private to avoid publishing to npm
			pkg.description = props.themename;
			pkg.author = 'Reason Digital <hello@reasondigital.com> (http://www.reasondigital.com/)';
			pkg.private = true;

			return pkg;
		});

		// Copy and replace paths in Gruntfile.js
		grunt.file.copy(init.destpath() + '/project/src/Gruntfile.js', init.destpath() + '/Gruntfile.js', {
			process: function(content, srcpath) {
				return content.replace(/{%= localurl %}/g, props.localurl).replace(/{%= srcdir %}/g, props.srcdir).replace(/{%= gruntpath %}/g, props.gruntpath);
			}
		});

		// Copy and replace paths in config.rb
		grunt.file.copy(init.destpath() + '/project/src/config.rb', init.destpath() + '/config.rb', {
			process: function(content, srcpath) {
				return content.replace(/{%= themeslug %}/g, props.themeslug);
			}
		});

		// Copy humans.txt
		grunt.file.copy(init.destpath() + '/project/src/humans.txt', init.destpath() + '/deploy/live/htdocs/humans.txt');

		// Copy theme screenshot
		grunt.file.copy(init.destpath() + '/project/src/screenshot.png', init.destpath() + '/' + props.gruntpath + '/screenshot.png');

		// Remove files on completion
		grunt.file.delete(init.destpath() + '/project/', {
			force: true
		});

		// All done!
		done();
	});
};
