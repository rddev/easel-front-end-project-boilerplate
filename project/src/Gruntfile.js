module.exports = function(grunt) {
	'use strict';

	require("jit-grunt")(grunt);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			js: {
				options: {
					separator: ';'
				},
				src: [
					'{%= srcdir %}/js/**/.js'
				],
				dest: '{%= gruntpath %}/js/scripts.js'
			},
		},
		uglify: {
			js: {
				files: {
					'{%= gruntpath %}/js/min/scripts.min.js': ['{%= gruntpath %}/js/scripts.js']
				}
			},
			options: {
				sourceMap: true,
				sourceMapIncludeSources: true
			}
		},
		jshint: {
			files: ['{%= srcdir %}/js/**/*.js'],
			options: {
				globals: {
					jquery: true
				}
			}
		},
		compass: {
			dist: {
				options: {
					config: 'config.rb'
				}
			}
		},
		uncss: {
			dist: {
				options: {
					ignore: [
						/label/,
						/input/,
						/select/,
						/textarea/,
						/--ignore$/
					],
					csspath: '../../../../{%= srcdir %}/css/',
					stylesheets: ['style.css']
				},
				files: {
					'{%= srcdir %}/css/clean.css': ['{%= gruntpath %}/**/**/*.php']
				}
			}
		},
		penthouse: {
			extract: {
				url: '{%= localurl %}',
				css: '{%= srcdir %}/css/clean.css',
				outfile: '{%= srcdir %}/css/style.css',
				width : 1300,
				height : 750
			}
		},
		cssmin: {
			my_target: {
				options: {
					keepSpecialComments: 0,
					advanced: false
				},
				files: [{
					expand: true,
					cwd: '{%= srcdir %}/css/',
					src: ['**/*.css', '**/!*.min.css'],
					dest: '{%= gruntpath %}/css/',
					ext: '.min.css'
				}]
			}
		},
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: '{%= srcdir %}/images',
					src: ['**/*.{png,jpg,gif}'],
					dest: '{%= gruntpath %}/images'
				}]
			}
		},
		svgmin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: '{%= srcdir %}/svg',
					src: ['**/*.svg'],
					dest: '{%= gruntpath %}/svg'
				}]
			}
		},
		clean: {
			images: ['{%= gruntpath %}/images'],
			svg: ['{%= gruntpath %}/svg']
		},
		watch: {
			js: {
				files: ['{%= srcdir %}/js/**/*.js'],
				tasks: ['jshint', 'concat:js', 'uglify:js'],
				options: {
					atBegin: true,
					livereload: true
				}
			},
			css: {
				files: ['{%= srcdir %}/**/*.scss', '{%= gruntpath %}/**/*.php'],
				tasks: ['compass', 'uncss', 'penthouse', 'cssmin'],
				options: {
					atBegin: true,
					livereload: true
				}
			},
			images: {
				files: ['{%= srcdir %}/images/**/*.{png,jpg,gif}'],
				tasks: ['clean:images', 'imagemin'],
				options: {
					atBegin: true,
					livereload: true
				}
			},
			svgmin: {
				files: ['{%= srcdir %}/svg/**/*.svg'],
				tasks: ['clean:svg', 'svgmin'],
				options: {
					atBegin: true,
					livereload: true
				}
			}
		}
	});

	grunt.registerTask('default', ['watch']);
};
