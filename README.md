Project Boilerplate
===================
> Initialise [project scaffolding](http://gruntjs.com/project-scaffolding) and deployment folders for a new project.

> Includes:
> - Jenkins-ready folder structure
> - Grunt project scaffolding
> - Underscores boilerplate theme

What does it do?
----------------

When run, the Project Boilerplate will:

- Prompt you for your local project URL, the project name, folder slug and description. It'll take care of the directory paths for Grunt and Compass, and also namespace your theme functions in the Underscores theme too.
- Create all the folders you need for Jenkins deployment.
- Include .gitignore, exclusions.txt and robots.txt for both Development and Staging to avoid your project being accidentally indexed by search engines on non-live instances.
- Create the `package.json`, `Gruntfile.js` and `config.rb` in the project root, ready for installation of necessary `node_modules` and running the project Grunt tasks.
- Remove itself after completion, so you don't have any unnecessary files clogging up your project.

What doesn't it do?
-------------------

Include / configure any WordPress core files - grab the latest version and add it to your project repo beforehand. This will be introduced at a later date though.

How do I use it?
----------------

1. Make sure `grunt-init` is installed (`npm install -g grunt-init`)
2. Navigate to your project directory in your terminal and run:

    > `curl -o boilerplate.zip https://bitbucket.org/rddev/easel-front-end-project-boilerplate/get/master.zip && tar --strip-components=1 -xf boilerplate.zip && rm boilerplate.zip`

3. Once completed, initialise by running:

    > `grunt-init project`

4. Enter the necessary details into the prompts.

How do I use Grunt?
----------------

Once you have run the Project Boilerplate, start using Grunt by running `npm install` in the root directory of your project and then run `grunt`. This will begin watching the files in the source directory (`/src/PROJECT`) and output to the destination directory when modified (`/htdocs/wp-content/themes/PROJECT`).

The Project Boilerplate includes the following Grunt tasks, runs and watches when `grunt` is run:

* `css`: Compiles CSS with Compass in `/src/PROJECT/sass`, runs UnCSS and Penthouse and outputs the files to `/src/PROJECT/css` and finally outputs the minfied files to `/htdocs/wp-content/themes/PROJECT/css`.
* `js`: Lints the JavaScript in `/src/PROJECT/js`, outputs the concatenated and minified files to `/htdocs/wp-content/themes/PROJECT/js`.
* `images`: Minifies image files in `/src/PROJECT/images` outputs them to `/htdocs/wp-content/themes/PROJECT/images`.
* `svgmin`: Minifies SVG files in `/src/PROJECT/svg` outputs them to `/htdocs/wp-content/themes/PROJECT/svg`.

Suggestions? Improvements? Bugs? Thought of a snappier name?
------------

If you want to see anything added or improved in the Project Boilerplate or want to report a bug, [add an issue](http://git.reasondigital.com/reason/boilerplate/issues) or email
[frontend.team@reasondigital.com](mailto:frontend.team@reasondigital.com).